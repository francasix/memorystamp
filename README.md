# Memory Stamp #

This is an external space memory using fancy flags and stamps
``

## Installation ##

Create config file

```
$ touch /server/db.config.js

$ touch /src/db.config.js
```

```js
module.exports = {
    host: 'xx.xxx.xx.xxx',
};
```

## Commands ##

### start frontend

```
nodemon ./bin/www
```

### start backend

```
redis-server
 
nodemon server/index.js
```

## API Connection ##

This project uses [flagada API](https://bitbucket.org/francasix/flagada/src/master/)

## Warning ##

Don't forget to compile `App.scss

## TODO ##

* condition list flag/stamps to current user