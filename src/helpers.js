export const asyncLoop = async ({ array, key, callback, param = null }) => {
    if (array) {
        const promises = array.map(async (item) => {
            if (!callback) {
                return item;
            }
            return await callback(key ? item[key] : item, param);
        });
        return await Promise.all(promises);
    }
};

export const filterLoop = (array, key, filterValue) => {
    return array.map(elm => {
        return elm.filter((e) => {
            return e[key] === filterValue;
        });
    }).filter(x => !!x.length);
};