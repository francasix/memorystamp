import React from 'react';

const StampComponent = ({ title, colour }) => {
    const { hexacode, luminance } = colour;
    return <div className="stamp"
                style={{ background: hexacode, color: parseFloat(luminance) ? '#000' : '#fff' }}>{title}</div>;
};
export default StampComponent;