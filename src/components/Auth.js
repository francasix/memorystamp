import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Cookie from 'js-cookie';

export class Login extends Component {
    state = { 'email': '', 'password': '', 'redirect': null, 'error': null };

    async handleSubmit(e) {
        e.preventDefault();
        const { email, password } = this.state;
        const response = await fetch('http://localhost:3001/auth/login', {
            method: 'POST',
            body: JSON.stringify({ email, password }),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (response.ok) {
            const data = await response.json();
            this.setState({ 'redirect': data.redirect, 'email': '', 'password': '' });
            Cookie.set('refreshToken', data.refreshToken);
            Cookie.set('accessToken', data.accessToken);
        } else {
            const data = await response.json();
            this.setState({ 'error': data.msg, 'email': '', 'password': '' });
        }

    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value.trim() });
    }

    render() {
        const { email, password, redirect, error } = this.state;
        if (redirect) {
            return <Redirect to={redirect}/>;
        }

        return <AuthWrapper message={error}>
            <form onSubmit={this.handleSubmit.bind(this)}>
                <input onChange={this.handleChange.bind(this)} type='ext' value={email} placeholder='email'
                       name='email'/><br/>
                <input onChange={this.handleChange.bind(this)} type='text' value={password} placeholder='password'
                       name='password'/><br/>
                <button>Login</button>
            </form>
        </AuthWrapper>;
    }
}

export class Register extends Component {
    state = { 'email': '', 'name': '', 'password': '', 'redirect': null, 'error': null };

    async handleSubmit(e) {
        e.preventDefault();
        const { email, name, password } = this.state;
        const response = await fetch('http://localhost:3001/auth/register', {
            method: 'POST',
            body: JSON.stringify({ email, name, password }),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (response.ok) {
            const data = await response.json();
            this.setState({ 'redirect': data.redirect, 'email': '', 'name': '', 'password': '' });
            Cookie.set('refreshToken', data.refreshToken);
            Cookie.set('accessToken', data.accessToken);
        } else {
            const data = await response.json();
            this.setState({ 'error': data.msg, 'email': '', 'name': '', 'password': '' });
        }
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value.trim() });
    }

    render() {
        const { name, email, password, error, redirect } = this.state;

        if (redirect) {
            return <Redirect to={redirect}/>;
        }

        return <AuthWrapper message={error}>
            <form onSubmit={this.handleSubmit.bind(this)}>
                <input onChange={this.handleChange.bind(this)} type='ext' value={email} placeholder='email'
                       name='email'/><br/>
                <input onChange={this.handleChange.bind(this)} type='text' value={name} placeholder='name'
                       name='name'/><br/>
                <input onChange={this.handleChange.bind(this)} type='text' value={password} placeholder='password'
                       name='password'/><br/>
                <button>Register</button>
            </form>
        </AuthWrapper>;
    }
}

export class AuthWrapper extends Component {
    state = { 'redirect': null, 'error': null };

    constructor(props) {
        super(props);
        this.styles = {
            color: '#E52B50'
        };
    }

    async logout() {

        const response = await fetch('http://localhost:3001/auth/logout', {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${Cookie.get('refreshToken')}`,
                'Content-Type': 'application/json'
            }
        });
        if (response.ok) {
            const data = await response.json();
            this.setState({ 'redirect': data.redirect });
            Cookie.remove('refreshToken');
            Cookie.remove('accessToken');
        }

    }

    render() {
        const { redirect } = this.state;
        const { children, message } = this.props;

        if (redirect) {
            return <Redirect to={redirect}/>;
        }
        return <div>
            {children}
            {message ? <div><p style={this.styles}>{message}</p></div> : null}
            <button onClick={this.logout.bind(this)}>Logout</button>
        </div>;
    }
}