import React, { useState, useLayoutEffect } from 'react';
import Stamp from '../services/Stamp';
import ColorPicker from 'react-colorful';
import 'react-colorful/dist/index.css';
import StampComponent from './Stamp';
import memoryStore from '../store/stampStore';

const StampForm = () => {
    const [memoryState, updateMemoryState] = useState(memoryStore.initialState);
    const [title, updateTitle] = useState('');
    const [colourPicker, colourPickerSwitch] = useState(false);
    const [colour, setColour] = useState('#b32aa9');

    useLayoutEffect(() => {
        memoryStore.subscribe(updateMemoryState);
        memoryStore.init();
    }, []);

    const addStamp = (e) => {
        e.preventDefault();
        if (title && colour) {
            Stamp.create({ title, colour }).then((stamp) => {
                    colourPickerSwitch(false);
                    updateTitle('');
                    memoryStore.addStamp(stamp);
                }
            );
        } else {
            // TODO manage error
        }
    };

    return <div>
        <input type='checkbox' checked={colourPicker} onChange={(e) => colourPickerSwitch(e.target.checked)}/>
        <input type="text" placeholder="stamp" value={title} onChange={(e) => updateTitle(e.target.value)}/>
        <button type='submit' onClick={addStamp}>add</button>

        {!!memoryState.stamps.length ?
            <ul>{memoryState.stamps.map(stamp => <StampComponent key={stamp.id} title={stamp.title}
                                                                 colour={stamp.colour}/>)}</ul> : null}

        {colourPicker ? <ColorPicker color={colour} onChange={setColour}/> : null}
    </div>;
};

export default StampForm;