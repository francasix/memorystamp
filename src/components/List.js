import React, { useEffect, useState } from 'react';
import { observer, inject } from 'mobx-react';
import Cookie from 'js-cookie';
import { Redirect } from 'react-router-dom';

const List = ({ store }) => {
    const { flags } = store;
    const [redirect, updateRedirect] = useState(null);

    // TODO manage loading
    const loading = false;

    useEffect(() => {
        async function checkToken() {
            const response = await fetch(`http://localhost:3001/token/check`, {
                method: 'POST',
                body: JSON.stringify({ refreshToken: Cookie.get('refreshToken') }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${Cookie.get('accessToken')}`
                }
            });

            const data = await response.json();

            if (!response.ok) {
                updateRedirect(data.redirect);
            } else {
                if (data.accessToken) {
                    Cookie.set('accessToken', data.accessToken);
                }
                store.getFlags();
            }
        }

        checkToken();

    }, [store]);


    return <AuthToken redirect={redirect}>
        <ul>{!loading ? flags.length > 0 ? flags.map((flag) => <li
                key={flag.id}>{flag.title}</li>) : null
            : <p>Loading...</p>}
        </ul>
    </AuthToken>;
};

const AuthToken = (props) => {
    return props.redirect ? <Redirect to={props.redirect}/> : props.children;
};

export default inject(({ store }) => ({ store }))(observer(List));