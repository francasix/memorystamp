import React from 'react';
import {Link} from 'react-router-dom';

const SwitchNavigator = (props) => {
    const {children, switchTo} = props;

    return <div>
        {children}
        <button className="switch-button">
            <Link to={switchTo}>switch</Link>
        </button>
    </div>;
};

export default SwitchNavigator;