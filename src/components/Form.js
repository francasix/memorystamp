import React, { useState, useLayoutEffect } from 'react';
import StampForm from './StampForm';
import memoryStore from '../store/stampStore';
import Flag from '../services/Flag';

const Form = () => {
    const [memoryState, updateMemoryState] = useState(memoryStore.initialState);
    const [title, updateTitle] = useState('');

    useLayoutEffect(() => {
        memoryStore.subscribe(updateMemoryState);
        memoryStore.init();
    }, []);

    const addFlag = (e) => {
        e.preventDefault();
        Flag.create({ stamps: memoryState.stamps, title });
        updateTitle('');
        memoryStore.clearStamp();
    };

    return <form>
        <input type="text" placeholder="flag" value={title} onChange={(e) => updateTitle(e.target.value)}/>
        <StampForm/>
        <button type='submit' onClick={addFlag}>create</button>
    </form>;
};

export default Form;