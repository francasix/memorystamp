import React, { Fragment } from 'react';
import './style/App.css';
import List from './components/List';
import { Register, Login } from './components/Auth';
import Form from './components/Form';
import SwitchNavigator from './components/SwitchNavigator';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

const RegisterComponent = () => (
    <Fragment>
        <SwitchNavigator switchTo='/login'>
            <Register/>
        </SwitchNavigator>
    </Fragment>
);

const LoginComponent = () => (
    <Fragment>
        <SwitchNavigator switchTo='/'>
            <Login/>
        </SwitchNavigator>
    </Fragment>
);

const ListComponent = () => (
    <Fragment>
        <SwitchNavigator switchTo='/form'>
            <List/>
        </SwitchNavigator>
    </Fragment>
);

const FormComponent = () => (
    <Fragment>
        <SwitchNavigator switchTo='/list'>
            <Form/>
        </SwitchNavigator>
    </Fragment>
);

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/" exact component={RegisterComponent}/>
                <Route path="/login" exact component={LoginComponent}/>
                <Route path="/list" exact component={ListComponent}/>
                <Route path="/form" exact component={FormComponent}/>
            </Switch>
        </Router>
    );
}

export default App;
