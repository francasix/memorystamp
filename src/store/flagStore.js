import { observable, action, decorate } from 'mobx';
import DataService from '../services/DataService';

export default class FlagStore {
    flags = [];

    async getFlags() {
        this.flags = await DataService.flags();
    }
}

decorate(FlagStore, {
    flags: observable,
    getFlags: action,
    getJoint: action,
});