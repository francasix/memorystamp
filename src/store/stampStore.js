import { Subject } from 'rxjs';

const subject = new Subject();
const initialState = {
    flags: [],
    stamps: [],
    flagTitle: ''
};

let state = initialState;

const memoryStore = {
    init: () => subject.next(state),
    subscribe: setState => subject.subscribe(setState),

    addStamp: stamp => {
        state = {
            ...state,
            stamps: [...state.stamps, stamp],
        };
        subject.next(state);
    },
    clearStamp: () => {
        state = {
            ...state,
            stamps: [],
        };
        subject.next(state);
    },
    initialState
};

export default memoryStore;