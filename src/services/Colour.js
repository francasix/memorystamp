import axios from 'axios';
import { colour } from '../project.config';

class Colour {
    async byId(id, sync = true) {
        const response = await axios.get(`http://localhost:8080/api/colours/${id}`);
        const { hexacode } = response.data || colour.default;
        return sync ? { hexacode, luminance: await this.getLuminance(hexacode) } : { hexacode };
    }

    async getLuminance(hexacode) {
        let hex = '', luminance = null, response, hsl;

        if (hexacode) {
            hex = hexacode.replace(/#/g, '');
            response = await axios.get(`https://www.thecolorapi.com/id?hex=${hex}`);
            hsl = response.data.hsl;
        }

        if (hsl) {
            luminance = hsl.l > 50 ? 1 : 0;
        }
        return luminance;
    }

    async postColour(params) {
        const { hexacode, luminance } = params;
        const response = await axios.put(`http://localhost:8080/api/colours/`, {
            hexacode,
            luminance: luminance.toString()
        });

        return { hexacode: response.data[0].hexacode, luminance: response.data[0].luminance, id: response.data[0].id };
    }
}

export default new Colour();