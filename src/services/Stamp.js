import axios from 'axios';
import Colour from './Colour';

const Stamp = {

    getById: async (id, sync) => {
        const response = await axios.get(`http://localhost:8080/api/stamps/${id}`);

        return sync ? { ...response.data, colour: Colour.byId(response.data.id) } : response;
    },

    getByIds: async (ids) => {
        return await axios.post(`http://localhost:8080/api/stamps/ids`, { ids });
    },

    create: async ({ title, colour }, sync = true) => {

        const luminance = await Colour.getLuminance(colour);
        const newColour = await Colour.postColour({ 'hexacode': colour, luminance });
        let newStamp = await Stamp._postStamp({ title, colour_id: newColour.id });

        if (sync) {
            newStamp = { ...newStamp, colour: newColour };
        }

        return newStamp;
    },

    _postStamp: async (params) => {
        let { title, colour_id } = params;
        const response = await axios.post(`http://localhost:8080/api/stamps/`, { title, colour_id });

        return { title: response.data.title, colour_id: response.data.colour_id, id: response.data.id };

    }
};

export default Stamp;