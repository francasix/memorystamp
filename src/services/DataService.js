import Flag from './Flag';
import Stamp from './Stamp';

const DataService = {

    /**
     * Fetch flags with their stamps
     * @returns {Promise<*>}
     */
    flags: async () => {
        let flags = [];

        return await Flag._getFlags().then(async (res) => {
            flags = await res.data;

            return await Promise.all(flags.map(async (flag) => {

                // get joint data
                const joints = await Flag._getJointByFlagId(flag.id);

                // return joint and current flag
                return { joint: joints.data, flag };

            })).then(async (response) => {

                // from joint data
                return await Promise.all(response.map(async (res) => {

                    const ids = res.joint.map((joint) => joint.stamp_id);

                    // get corresponding stamps to current flag
                    const stamps = await Stamp.getByIds(ids);
                    return { ...res.flag, stamps: stamps.data };

                })).then((response) => {
                    return response;
                });
            });
        }).then((res) => res);
    }
};

export default DataService;