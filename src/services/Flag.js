import axios from 'axios';
import { asyncLoop as loop } from '../helpers';
import Cookie from 'js-cookie';
const config = require('../db.config.js');

axios.defaults.headers.common['Authorization'] = Cookie.get('refreshToken');
const Flag = {

    _postFlag: async (title) => {
        return await axios.post(`http://${config.host}:8080/api/flags`, { title });
    },

    _getFlags: async () => {
        return await axios.get(`http://${config.host}:8080/api/flags`);
    },

    _getJointByFlagId: async (id) => {
        return await axios.get(`http://${config.host}:8080/api/flag_stamps/flag/${id}`);
    },

    _postFlagStamp: async (stamp, flagId) => {
        const response = await axios.post(`http://${config.host}:8080/api/flag_stamps`, {
            flag_id: flagId,
            stamp_id: stamp.id
        });
        return response.data;
    },

    create: async ({ title, stamps }) => {
        const newFlag = await Flag._postFlag(title);
        const joint = await loop({
            array: stamps,
            param: newFlag.data.id,
            callback: await Flag._postFlagStamp
        });

        await Promise.all([newFlag, joint]).catch(err => err);
    }
};

export default Flag;
