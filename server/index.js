const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const corsOption = {
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
    exposedHeaders: ['x-auth-token']
};

app.use(bodyParser.json());
app.use(cors(corsOption));

require('./routes/register')(app);
require('./routes/login')(app);
require('./routes/logout')(app);
require('./routes/token')(app);

app.listen(3001, () => console.log('Server is Running on 3001...'));