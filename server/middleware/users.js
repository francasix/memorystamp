const accessTokenSecret = 'thisisafuckingaccesstokensecret';
const refreshTokenSecret = 'thisisafuckingrefreshtokensecret';
const jwt = require('jsonwebtoken');
const axios = require('axios');

module.exports = {
    validateRegister: (req, res, next) => {
        // name min length 3
        if (!req.body.name || req.body.name.length < 3) {
            return res.status(400).send({
                msg: 'Please enter a name with min. 3 chars'
            });
        }
        // password min 6 chars
        if (!req.body.password || req.body.password.length < 6) {
            return res.status(400).send({
                msg: 'Please enter a password with min. 6 chars'
            });
        }
        next();
    },
    isTokenValid: (req, res, next) => {
        const refreshToken = req.body.refreshToken;

        if (refreshToken) {

            axios.post(`http://localhost:8080/api/token/name`, { name: refreshToken }).then((response) => {
                if (!response.data.length) {
                    next();
                } else {
                    // Token blacklisted
                    return res.status(403).send({
                        msg: 'Token unauthorized',
                        redirect: '/'
                    });
                }
            });
        } else {
            return res.status(401).send({
                msg: 'Token is missing',
                redirect: '/'
            });
        }
    }
};