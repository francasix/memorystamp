const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const userMiddleware = require('../middleware/users.js');
const accessTokenSecret = 'thisisafuckingaccesstokensecret';
const refreshTokenSecret = 'thisisafuckingrefreshtokensecret';

module.exports = app => {

    router.post('/check', userMiddleware.isTokenValid, (req, res) => {
        const refreshToken = req.body.refreshToken;
        const authHeader = req.headers.authorization;
        let newAccessToken = '';

        if (refreshToken && authHeader) {
            const accessToken = authHeader.split(' ')[1];


            jwt.verify(refreshToken, refreshTokenSecret, (err, user) => {
                const tokenUser = user;
                if (err) {
                    return res.status(403).send({
                        response: err,
                        redirect: '/login'
                    });
                } else {
                    jwt.verify(accessToken, accessTokenSecret, (err, user) => {
                        console.log('==== verify ==== ', err, user);
                        if (err) {
                            if (err.message === 'jwt expired') {
                                newAccessToken = jwt.sign({ email: tokenUser.email }, accessTokenSecret, { expiresIn: '1m' });
                                req.body.accessToken = accessToken;
                            }
                        }

                        const send = { response: 'success' };
                        if (newAccessToken) {
                            send.accessToken = newAccessToken;
                        }
                        return res.status(200).send(send);
                    });
                }
            });

        } else {
            return res.status(401).send({
                msg: 'Token missing',
                redirect: '/login'
            });

        }
    });

    app.use('/token/', router);
};