const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const axios = require('axios');
const accessTokenSecret = 'thisisafuckingaccesstokensecret';
const refreshTokenSecret = 'thisisafuckingrefreshtokensecret';
const config = require('../../src/db.config.js');

module.exports = app => {
    router.post('/login', (req, res, next) => {

        axios.post(`http://${config.host}:8080/api/users/email`, { email: req.body.email }).then((response) => {

            // email does not exists
            if (!response.data.length) {
                return res.status(401).send({
                    msg: 'Email incorrect!'
                });
            }

            // check password
            bcrypt.compare(
                req.body.password,
                response.data[0]['password'],
                (bErr, bResult) => {
                    // wrong password
                    if (bErr) {
                        throw bErr;
                        return res.status(401).send({
                            msg: 'Password is incorrect!'
                        });
                    }
                    if (bResult) {
                        const accessToken = jwt.sign({ email: response.data[0].email }, accessTokenSecret, { expiresIn: '1m' });
                        const refreshToken = jwt.sign({ email: response.data[0].email }, refreshTokenSecret);

                        return res.status(201).send({
                            response: 'LoggedIn!',
                            redirect: '/list',
                            accessToken,
                            refreshToken
                        });
                    }
                    return res.status(401).send({
                        msg: 'Email or password is incorrect!'
                    });
                }
            );
        }).catch((err) => {
            throw err;
            return res.status(400).send({
                msg: err
            });

        });
    });
    app.use('/auth/', router);
};