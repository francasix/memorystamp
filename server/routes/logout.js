const express = require('express');
const axios = require('axios');
const router = express.Router();
const userMiddleware = require('../middleware/users.js');
const jwt = require('jsonwebtoken');
const refreshTokenSecret = 'thisisafuckingrefreshtokensecret';
const accessTokenSecret = 'thisisafuckingaccesstokensecret';
const config = require('../../src/db.config.js');

module.exports = app => {
    router.post('/logout', (req, res, next) => {
        const authHeader = req.headers.authorization;
        const bearer = authHeader && authHeader.split(' ')[0];
        const token = authHeader && authHeader.split(' ')[1];

        if (bearer !== 'Bearer') {
            return res.sendStatus(401);
        }

        if (!token) {
            return res.sendStatus(401);
        }

        axios.post(`http://${config.host}:8080/api/token`, { token }).then((response) => {

        }).then((response) => {
            return res.status(201).send({
                response: 'LoggedOut!',
                redirect: '/login',
            });

        });
    });
    app.use('/auth/', router);
};