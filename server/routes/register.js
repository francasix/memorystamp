const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const userMiddleware = require('../middleware/users.js');
const axios = require('axios');
const accessTokenSecret = 'thisisafuckingaccesstokensecret';
const refreshTokenSecret = 'thisisafuckingrefreshtokensecret';
const config = require('../../src/db.config.js');

module.exports = app => {

    router.post('/register', userMiddleware.validateRegister, (req, res, next) => {

        axios.post(`http://${config.host}:8080/api/users/email`, { email: req.body.email }).then((response) => {

            if (response.data.length) {
                return res.status(409).send({
                    msg: 'This email is already in use!'
                });
            } else {
                // email is available
                bcrypt.genSalt(2, (err, salt) => {

                    bcrypt.hash(req.body.password, salt, (err, hash) => {

                        if (err) {
                            return res.status(500).send({
                                msg: err
                            });
                        } else {

                            // user creation
                            axios.post(`http://${config.host}:8080/api/users/`, {
                                    name: req.body.name,
                                    email: req.body.email,
                                    password: hash
                                }
                            ).then((response) => {

                                const accessToken = jwt.sign({ email: response.data.email }, accessTokenSecret, { expiresIn: '1m' });
                                const refreshToken = jwt.sign({ email: response.data.email }, refreshTokenSecret);

                                return res.status(201).send({
                                    response: 'Registered!',
                                    redirect: '/list',
                                    accessToken,
                                    refreshToken
                                });
                            }).catch((err) => {
                                if (err) {
                                    throw err;
                                    return res.status(400).send({
                                        msg: err
                                    });
                                }
                            });
                        }
                    });
                });
            }
        }).catch((err) => {
            if (err) {
                throw err;
                return res.status(400).send({
                    msg: err
                });
            }
        });
    });
    app.use('/auth/', router);
};